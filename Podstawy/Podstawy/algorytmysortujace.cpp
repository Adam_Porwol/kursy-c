#include "Includes.h"

double sortowanie_babelkowe(int* t, int size)
{
	clock_t start, stop;
	start = clock();
	for (int i = 1; i < size; i++)
	{
		for (int j = size - 1; j >= 1; j--)
		{
			if (t[j] < t[j - 1])
			{
				int bufor = t[j - 1];
				t[j - 1] = t[j];
				t[j] = bufor;
			}
		}
	}
	stop = clock();
	return (double)(stop - start) / CLOCKS_PER_SEC;
}

void sortowanie_quicksort(int* t, int p, int q)
{
	int v = t[(p + q) / 2];
	int i, j, bufor;
	i = p;
	j = q;
	do
	{
		while (t[i] < v)
			i++;
		while (t[j] > v)
			j--;
		if (i <= j)
		{
			bufor = t[i];
			t[i] = t[j];
			t[j] = bufor;
			i++;
			j--;
		}
	} while (i <= j);
	if (j > p) sortowanie_quicksort(t, p, j);
	if (i < q) sortowanie_quicksort(t, i, q);
}

void algorytmysortujace(int size)
{
	int* tab = generaterandomtab(size);
	/*for (int i = 0; i < size; i++)
		cout << tab[i] << " ";*/
	cout << "\n" << sortowanie_babelkowe(tab, size) << endl;
	/*for (int i = 0; i < size; i++)
		cout << tab[i] << " ";*/
	tab = generaterandomtab(size);
	clock_t start, stop;
	start = clock();
	sortowanie_quicksort(tab, 0, size - 1);
	stop = clock();
	cout << "\n" << (double)(stop - start) / CLOCKS_PER_SEC << endl;
}