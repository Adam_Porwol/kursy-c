#pragma once

#include <iostream>
#include <cstdlib>
#include <time.h>
#include <iomanip>
#include <fstream>
#include <string>
#include <list>

using namespace std;

void fibonacci();
void pseudolosowe();
void pliki(string imie, string nazwisko, int telefon);
void lancuchy(string imie);
void compare(int ile);
double srednia(int* t, int ile);
void srednia2(int ile);
int* generaterandomtab(int ile);
void maksimum(int ile);
void najblizszadosredniej(int ile);
void rekurencja();
void algorytmysortujace(int size);
void stos();
void kolejka();
void lista();
void drzewo();