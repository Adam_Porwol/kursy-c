#include "Includes.h"

void fibonacci()
{
	double fib[100];
	fib[0] = fib[1] = 1;
	cout << setprecision(100);

	for (int i = 2; i < 100; i++)
	{
		fib[i] = fib[i - 1] + fib[i - 2];
	}
	cout << fib[99];
	getchar();
}