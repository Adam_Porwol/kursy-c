#include "Includes.h"

int potega(int p, int w)
{
	if (w == 0) return 1;
	else return p * potega(p, w - 1);
}

int fib(int n)
{
	if (n == 1 || n == 2) return 1;
	else return fib(n - 1) + fib(n - 2);
}

int silnia(int n)
{
	if (n == 1 || n == 0) return 1;
	else return n * silnia(n - 1);
}

void rekurencja()
{
	cout << potega(5, 5) << endl;
	cout << fib(10) << endl;
	cout << silnia(6) << endl;
}