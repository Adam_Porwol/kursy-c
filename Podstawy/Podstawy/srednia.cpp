#include "Includes.h"

double srednia(int* t, int ile)
{
	int sum = 0;
	for (int i = 0; i < ile; i++)
	{
		sum += *t;
		t++;
	}
	return (double)sum / ile;
}