#include "Includes.h"

static void wyswietl(list<int>& lista)
{
	system("CLS");
	for (list<int>::iterator i = lista.begin(); i != lista.end(); ++i)
		cout << *i << " ";
	cout << endl;
}

static void push_front(list<int>& lista)
{
	lista.push_front(rand() % 10);
}

static void push_back(list<int>& lista)
{
	lista.push_back(rand() % 10);
}

static void pop_front(list<int>& lista)
{
	if (!lista.empty())lista.pop_front();
}

static void pop_back(list<int>& lista)
{
	if (!lista.empty())lista.pop_back();
}

static void size(list<int>& lista)
{
	cout << lista.size() << endl;
}

static void max_size(list<int>& lista)
{
	cout << lista.max_size() << endl;
}

static void empty(list<int>& lista)
{
	if (lista.empty())
		cout << "True";
	else
		cout << "False";
}

static void remove(list<int>& lista, int n)
{
	if (!lista.empty())lista.remove(n);
}

static void sort(list<int>& lista)
{
	lista.sort();
}

static void reverse(list<int>& lista)
{
	lista.reverse();
}

void lista()
{
	list<int> lista;
	int select, n;
	srand(time(NULL));
	do
	{
		wyswietl(lista);
		cin >> select;

		switch (select)
		{
		case 1:
			push_front(lista);
			break;
		case 2:
			push_back(lista);
			break;
		case 3:
			pop_front(lista);
			break;
		case 4:
			pop_back(lista);
			break;
		case 5:
			size(lista);
			break;
		case 6:
			max_size(lista);
			break;
		case 7:
			empty(lista);
			break;
		case 8:
			cin >> n;
			remove(lista, n);
			break;
		case 9:
			sort(lista);
			break;
		case 0:
			reverse(lista);
			break;
		default:
			break;
		}
	} while (select != 10);
}