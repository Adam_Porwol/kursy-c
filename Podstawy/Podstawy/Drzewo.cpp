//https://www.geeksforgeeks.org/binary-search-tree-insert-parent-pointer/

#include "Includes.h"

struct treeElement
{
	int val = 0;
	struct treeElement* leftChild = NULL;
	struct treeElement* rightChild = NULL;
	struct treeElement* parent = NULL;
};

static void print(struct treeElement* root)
{
	if (root != NULL)
	{
		print(root->leftChild);
		cout << "Node: " << root->val;
		if (root->parent == NULL)
			cout << " Parent: NULL\n";
		else
			cout << " Parent: " << root->parent->val << endl;
		print(root->rightChild);
	}
}

struct treeElement* add(struct treeElement* node, int val)
{
	if (node == NULL)
	{
		struct treeElement *node = new treeElement;
		node->leftChild = NULL;
		node->rightChild = NULL;
		node->parent = NULL;
		node->val = val;
		return node;
	}
	if (val < node->val)
	{
		treeElement* leftChild = add(node->leftChild, val);
		node->leftChild = leftChild;
		leftChild->parent = node;
	}
	else if (val > node->val)
	{
		treeElement* rightChild = add(node->rightChild, val);
		node->rightChild = rightChild;
		rightChild->parent = node;
	}
	return node;
}

void drzewo()
{
	struct treeElement* root = NULL;
	root = add(root, 50);
	add(root, 30);
	add(root, 20);
	add(root, 40);
	add(root, 70);
	add(root, 60);
	add(root, 80);

	print(root);
}