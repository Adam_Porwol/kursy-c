#include "Includes.h"

static void wyswietl(int* dane, int rozmiar)
{
	system("CLS");
	if (rozmiar != 0)
	{
		for (int i = rozmiar; i >= 1; i--)
			cout << dane[i] << " ";
	}
	else
		cout << "Pusty";
	cout << endl;
}

static void push(int* dane, int& rozmiar)
{
	if (rozmiar >= 5)
		cout << "Pelny!";
	else
	{
		rozmiar++;
		dane[rozmiar] = rand() % 10;
	}
}

static void pop(int* dane, int& rozmiar)
{
	if (rozmiar >= 1)
	{
		for (int i = 1; i <= rozmiar; i++)
			dane[i] = dane[i + 1];
		rozmiar--;
	}
	else
		cout << "Pusty!";
}

static void size(int rozmiar)
{
	cout << rozmiar << " elementow";
}

static void empty(int rozmiar)
{
	if (rozmiar == 0)
		cout << "True";
	else
		cout << "False";
}

void kolejka()
{
	int dane[11], rozmiar = 0;
	int select;
	srand(time(NULL));
	do
	{
		wyswietl(dane, rozmiar);
		cin >> select;

		switch (select)
		{
		case 1:
			push(dane, rozmiar);
			break;
		case 2:
			pop(dane, rozmiar);
			break;
		case 3:
			size(rozmiar);
			break;
		case 4:
			empty(rozmiar);
		default:
			break;
		}
	} while (select != 5);
}