#include "Includes.h"

void pliki(string imie, string nazwisko, int telefon)
{
	struct wizytowka
	{
		string imie, nazwisko;
		int telefon;
	};

	wizytowka pracownik;
	pracownik.imie = imie;
	pracownik.nazwisko = nazwisko;
	pracownik.telefon = telefon;

	fstream plik;
	plik.open("wizytowka.txt", ios::out | ios::app);
	plik << pracownik.imie << ", " << pracownik.nazwisko << ", " << pracownik.telefon << "\n";
	plik.close();

	fstream plik2;
	plik2.open("wizytowka.txt", ios::in);
	if (plik2.bad())
	{
		cout << "Nie znaleziono" << endl;
		exit(0);
	}
	else
	{
		string linia;
		while (getline(plik2, linia))
		{
			cout << linia << endl;
		}
	}
	plik2.close();
	getchar();
}